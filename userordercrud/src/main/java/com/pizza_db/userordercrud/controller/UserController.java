package com.pizza_db.userordercrud.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza_db.userordercrud.model.*;
import com.pizza_db.userordercrud.repository.*;


@RequestMapping("/")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)
public class UserController {
    @Autowired
    IUserRepository iUserRepository;
    
    @GetMapping("/users")
    public ResponseEntity<Object> getAllUsers() {
        try {
            return new ResponseEntity<>(iUserRepository.findAll(), HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(ex.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable long id) {
        Optional<CUser> foundUser = iUserRepository.findById(id);
        if(foundUser.isPresent()) {
            return new ResponseEntity<>(foundUser.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody CUser paramUser) {
        try {
            CUser newUser = new CUser();
            newUser.setFullName(paramUser.getFullName());
            newUser.setEmail(paramUser.getEmail());
            newUser.setPhone(paramUser.getPhone());
            newUser.setAddress(paramUser.getAddress());
            return new ResponseEntity<>(iUserRepository.save(newUser), HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
            .body("Failed to Create User: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable long id, @RequestBody CUser paramUser) {
        try {
            Optional<CUser> existedUser = iUserRepository.findById(id);
            if (existedUser.isPresent()) {
                CUser updatedUser = existedUser.get();
                updatedUser.setFullName(paramUser.getFullName());
                updatedUser.setEmail(paramUser.getEmail());
                updatedUser.setPhone(paramUser.getPhone());
                updatedUser.setAddress(paramUser.getAddress());
                return ResponseEntity.ok(iUserRepository.save(updatedUser));
            } else 
                return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Failed to Update User: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable long id) {
        try {
            Optional<CUser> existedUser = iUserRepository.findById(id);
            if(existedUser.isPresent()) {
                iUserRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else
                return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                .body("Failed to Delete User: " + e.getCause().getCause().getMessage());
        }
    }

    @GetMapping("/users/count")
    public long countUser() {
        return iUserRepository.count();
    }

    @GetMapping("/users/check/{id}")
    public boolean checkUserById(@PathVariable long id) {
        return iUserRepository.existsById(id);
    }
}
