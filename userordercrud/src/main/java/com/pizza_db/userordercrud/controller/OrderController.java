package com.pizza_db.userordercrud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pizza_db.userordercrud.model.*;
import com.pizza_db.userordercrud.repository.*;


@RequestMapping("/order")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)

public class OrderController { 
    @Autowired
    IUserRepository iUserRepository;
    @Autowired
    IOrderRepository iOrderRepository;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllOrders() {
        try {
            List<COrder> listOrder = new ArrayList<COrder>();
            iOrderRepository.findAll().forEach(listOrder::add);
            return new ResponseEntity<>(listOrder, HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/detail")
    public ResponseEntity<Object> getOrdersByUserId(@RequestParam long userId) {
        Optional<CUser> existedUser = iUserRepository.findById(userId);
        if(existedUser.isPresent()) {
            try {
                return new ResponseEntity<>(existedUser.get().getOrders(), HttpStatus.OK);
            } catch(Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable long id) {
        Optional<COrder> foundOrder = iOrderRepository.findById(id);
        if(foundOrder.isPresent()) {
            return new ResponseEntity<>(foundOrder.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createOrder(
    @RequestParam long userId, @RequestBody COrder paramOrder) {
        Optional<CUser> existedUser = iUserRepository.findById(userId);
        if(existedUser.isPresent()) {
            COrder newOrder = new COrder();
            newOrder.setOrderCode(paramOrder.getOrderCode());
            newOrder.setVoucherCode(paramOrder.getVoucherCode());
            newOrder.setPizzaSize(paramOrder.getPizzaSize());
            newOrder.setPizzaType(paramOrder.getPizzaType());
            newOrder.setPrice(paramOrder.getPrice());
            newOrder.setPaid(paramOrder.getPaid());
            newOrder.setUser(existedUser.get());
            try {
                return new ResponseEntity<>(iOrderRepository.save(newOrder), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable long id, @RequestBody COrder paramOrder) {
        Optional<COrder> existedOrderData = iOrderRepository.findById(id);
        if(existedOrderData.isPresent()) {
            COrder existedOrder = existedOrderData.get();
            existedOrder.setOrderCode(paramOrder.getOrderCode());
            existedOrder.setVoucherCode(paramOrder.getVoucherCode());
            existedOrder.setPizzaSize(paramOrder.getPizzaSize());
            existedOrder.setPizzaType(paramOrder.getPizzaType());
            existedOrder.setPrice(paramOrder.getPrice());
            existedOrder.setPaid(paramOrder.getPaid());
            try {
                return new ResponseEntity<>(iOrderRepository.save(existedOrder), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>("Order not found", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable long id) {
        Optional<COrder> existedOrder = iOrderRepository.findById(id);
        if(existedOrder.isPresent()) {
            try {
                iOrderRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete Order: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>("Order not found", HttpStatus.NOT_FOUND);
        }
    }

}
