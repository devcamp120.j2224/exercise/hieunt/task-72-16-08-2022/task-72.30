package com.pizza_db.userordercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza_db.userordercrud.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long>{
}
